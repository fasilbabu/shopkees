import 'package:flutter/material.dart';

const colorPrimary = Color(0xFF124151);
const colorDarkGray = Color(0xFF878787);
const colorWhite = Color(0xFFFFFFFF);

const spacing = 20.0;
