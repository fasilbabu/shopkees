import 'package:flutter/material.dart';
import 'package:shopkees/screens/home/home_screen.dart';
import 'package:shopkees/constants.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Shopkees',
      theme: ThemeData(
          textTheme: Theme.of(context).textTheme.apply(bodyColor: colorWhite),
          visualDensity: VisualDensity.adaptivePlatformDensity),
      home: HomeScreen(),
    );
  }
}
