import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shopkees/constants.dart';

AppBar buildAppBar() {
  return AppBar(
    backgroundColor: colorPrimary,
    leading: Container(
      width: 25,
      height: 25,
      padding: const EdgeInsets.fromLTRB(10.0, 0, 0, 0),
      margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: SvgPicture.asset(
        'assets/images/logo.svg',
        color: Colors.white,
      ),
    ),
    actions: <Widget>[
      Padding(
        padding: const EdgeInsets.symmetric(
          vertical: spacing / 4,
          horizontal: 0,
        ),
        child: Material(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(50.0),
          child: IconButton(
            icon: Icon(
              Icons.account_circle,
            ),
            onPressed: () {},
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(
          vertical: spacing / 4,
          horizontal: 0,
        ),
        child: Material(
          borderRadius: BorderRadius.circular(50.0),
          color: Colors.transparent,
          child: InkWell(
            child: IconButton(
              icon: Icon(
                Icons.shopping_cart,
              ),
              onPressed: () {},
            ),
          ),
        ),
      ),
    ],
    bottom: PreferredSize(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
          child: TextField(
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              hintText: 'Search',
              hintStyle: TextStyle(color: colorDarkGray),
            ),
          ),
        ),
        preferredSize: Size(0, 55)),
  );
}
