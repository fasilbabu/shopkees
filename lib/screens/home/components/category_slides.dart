import 'package:flutter/material.dart';
import 'package:shopkees/models/Category.dart';

class CategorySlides extends StatefulWidget {
  @override
  _CategorySlidesState createState() => _CategorySlidesState();
}

class _CategorySlidesState extends State<CategorySlides> {
  List<Category> categories;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: categories.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return Text(categories[index].title);
        });
  }
}
