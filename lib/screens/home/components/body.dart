import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:shopkees/constants.dart';
import 'package:shopkees/models/Product.dart';
import 'package:shopkees/screens/home/components/category_slides.dart';
import 'package:shopkees/screens/home/components/item_card.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          height: 200.0,
          child: Carousel(
            boxFit: BoxFit.cover,
            dotSize: 5.0,
            dotSpacing: 16.0,
            dotBgColor: Colors.transparent,
            images: [
              AssetImage('assets/images/laptops_banner.jpg'),
              AssetImage('assets/images/laptops_banner-2.jpg'),
            ],
          ),
        ),
        // CategorySlides(),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: spacing / 2),
            child: GridView.builder(
              itemCount: products.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: spacing,
                crossAxisSpacing: spacing,
                childAspectRatio: 0.75,
              ),
              itemBuilder: (context, index) => ItemCard(
                product: products[index],
              ),
            ),
          ),
        )
      ],
    );
  }
}
