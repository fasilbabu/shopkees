import 'package:flutter/material.dart';
import 'package:shopkees/screens/home/components/app_bar.dart';
import 'package:shopkees/screens/home/components/body.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: Body(),
    );
  }
}
