class Category {
  final String title, image;
  final int id;

  Category({
    this.id,
    this.title,
    this.image,
  });

  List<Category> categories = [
    Category(
      id: 1,
      title: "Laptop",
      image: "assets/images/laptop.png",
    ),
    Category(
      id: 2,
      title: "Mobile & Tabs",
      image: "assets/images/mobiles-and-accesorice.png",
    ),
    Category(
      id: 3,
      title: "Desktops",
      image: "assets/images/desktop_monitor.png",
    ),
    Category(
      id: 4,
      title: "Monitors",
      image: "assets/images/monitor.png",
    ),
    Category(
      id: 5,
      title: "Servers",
      image: "assets/images/servers.png",
    ),
    Category(
      id: 6,
      title: "Software",
      image: "assets/images/software.png",
    ),
    Category(
      id: 7,
      title: "Projector",
      image: "assets/images/Projecters_TV.png",
    ),
    Category(
      id: 8,
      title: "Accessories",
      image: "assets/images/accessories.png",
    ),
    Category(
      id: 9,
      title: "Networking",
      image: "assets/images/networking.png",
    )
  ];
}
