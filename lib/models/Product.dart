class Product {
  final String image, title, description;
  final int price, discount, id;

  Product({
    this.id,
    this.title,
    this.image,
    this.price,
    this.discount,
    this.description,
  });
}

List<Product> products = [
  Product(
    id: 1,
    title: "BenQ ZOWIE RL2755",
    image: "assets/images/benq-zowie.jpg",
    price: 1134,
    discount: 10,
    description: dummyText,
  ),
  Product(
    id: 2,
    title: "HP 290 G2 Microtower",
    image: "assets/images/HP-290-G2Microtower.jpg",
    price: 1134,
    discount: 12,
    description: dummyText,
  ),
  Product(
    id: 3,
    title: "Dell 20 Inch LED",
    image: "assets/images/dell-20-led.jpg",
    price: 1134,
    discount: 15,
    description: dummyText,
  ),
  Product(
    id: 4,
    title: "Dell Vostro",
    image: "assets/images/dell-vostro.jpg",
    price: 1134,
    discount: 17,
    description: dummyText,
  ),
  Product(
    id: 4,
    title: "BenQ 23.8-Inch LED Monitor",
    image: "assets/images/benq-23.jpg",
    price: 1135,
    discount: 20,
    description: dummyText,
  ),
  Product(
    id: 5,
    title: "Lenovo e73 Tower",
    image: "assets/images/lenovo-e73-tower.jpg",
    price: 2300,
    discount: 7,
    description: dummyText,
  )
];

String dummyText =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since. When an unknown printer took a galley.";
